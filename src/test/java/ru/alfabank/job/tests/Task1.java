package ru.alfabank.job.tests;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class Task1 {
	private List<Integer> numbers = new ArrayList<Integer>();

	@Before
	public void before() {
		if (!numbers.isEmpty()) {
			return;
		}

		try (BufferedReader br = new BufferedReader(
				new InputStreamReader(new FileInputStream("./TestFiles/numbers.txt")))) {
			String strLine;
			while ((strLine = br.readLine()) != null) {
				String[] splitedNumbers = strLine.split(",");
				for (String splitedNum : splitedNumbers) {
					numbers.add(Integer.parseInt(splitedNum));
				}
			}
		} catch (IOException e) {
			System.out.println("Ошибка");
		}
	}

	/**
	 * По возрастанию
	 */
	@Test
	public void testAscending() {
		Collections.sort(numbers);
		System.out.println(numbers);
	}

	/**
	 * По убыванию
	 */
	@Test
	public void testDescendingly() {
		Collections.sort(numbers, Collections.reverseOrder());
		System.out.println(numbers);
	}

}
