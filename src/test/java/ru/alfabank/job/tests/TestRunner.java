package ru.alfabank.job.tests;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features = "Feature", glue = { "ru.alfabank.job.stepDefinition" }, plugin = { "pretty",
		"html:TestFiles/Html/Alltest" }, monochrome = true)

public class TestRunner {

}
