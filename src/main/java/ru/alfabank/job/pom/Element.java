package ru.alfabank.job.pom;

import java.util.List;
import java.util.stream.Collectors;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Element {
	private static final int TIMEOUT = 30;
	private WebDriver driver;
	private By by;

	public Element(WebDriver driver, By by) {
		this.driver = driver;
		this.by = by;
	}

	public WebElement getWebElement() {
		waitForPresent();
		return driver.findElement(by);
	}

	public WebElement getDisplayedWebElement() {
		waitForDisplayed();
		return driver.findElements(by).stream().filter(el -> el.isDisplayed()).findAny().orElse(null);
	}

	public List<WebElement> getWebElements() {
		waitForPresent();
		return driver.findElements(by);
	}

	public List<String> getAllTexts() {
		List<WebElement> elements = getWebElements();
		return elements.stream().map(el -> el.getText().trim()).collect(Collectors.toList());
	}

	public String getClassAttr() {
		WebElement element = getWebElement();
		return element.getAttribute("class");
	}

	public void moveToElement() {
		WebElement element = getWebElement();
		Actions action = new Actions(driver);
		action.moveToElement(element).perform();
	}

	public void clickIfPresent() {
		if (!isElementDisplayed()) {
			return;
		}
		getDisplayedWebElement().click();
		waitForNotDisplayed();
	}

	public Boolean isElementPresent() {
		return driver.findElements(by).size() > 0;
	}

	public Boolean isElementDisplayed() {
		return driver.findElements(by).stream().anyMatch(el -> el.isDisplayed());
	}

	public void waitForPresent() {
		new WebDriverWait(driver, TIMEOUT, 100).until(wd -> !wd.findElements(by).isEmpty());
	}

	public void waitForNotPresent() {
		new WebDriverWait(driver, TIMEOUT, 100).until(wd -> wd.findElements(by).isEmpty());
	}

	public void waitForDisplayed() {
		new WebDriverWait(driver, TIMEOUT, 100)
				.until(wd -> wd.findElements(by).stream().anyMatch(el -> el.isDisplayed()));
	}

	public void waitForNotDisplayed() {
		new WebDriverWait(driver, TIMEOUT, 100)
				.until(wd -> !wd.findElements(by).stream().anyMatch(el -> el.isDisplayed()));
	}
}
