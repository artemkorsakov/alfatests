package ru.alfabank.job.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class GooglePage extends MainPage {
	public GooglePage(WebDriver driver) {
		super(driver);
	}

	public Element searchInput() {
		return new Element(driver, By.xpath("//input[@title='Поиск']"));
	}

	public Element clickFirstResult() {
		return new Element(driver, By.xpath("//div[@class='sbqs_c']"));
	}

	public Element alfaLink() {
		return new Element(driver, By.xpath("//a[.='Альфа-Банк – Альфа-Банк']"));
	}
}
