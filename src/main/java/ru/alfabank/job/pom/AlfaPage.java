package ru.alfabank.job.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AlfaPage extends MainPage {
	public AlfaPage(WebDriver driver) {
		super(driver);
	}

	public Element jobsLink() {
		return new Element(driver, By.xpath("//a[.='Вакансии']"));
	}

	public Element aboutWorkInBankLink() {
		return new Element(driver, By.xpath("//a[@class='nav_item-link'][.='О работе в банке']"));
	}

	public Element aboutWorkInBankTitle() {
		return new Element(driver, By.xpath("//h1[.='О работе в банке']"));
	}

	public Element message() {
		return new Element(driver, By.xpath("//div[@class='message']"));
	}

	public Element info() {
		return new Element(driver, By.xpath("//div[@class='info']"));
	}
}
