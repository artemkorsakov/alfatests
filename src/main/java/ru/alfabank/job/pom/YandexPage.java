package ru.alfabank.job.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class YandexPage extends MainPage {
	public YandexPage(WebDriver driver) {
		super(driver);
	}

	public Element cityLink() {
		return new Element(driver, By.xpath("//span[contains(@class, 'region-notification')][span[.='Да, спасибо']]"));
	}

	public Element promoLink() {
		return new Element(driver, By.xpath("//a[span[.='Начать покупки']]"));
	}

	public Element homeLink(String name) {
		return new Element(driver,
				By.xpath("//div[contains(@class, 'home-tabs')]/a[contains(@class, 'home-link')][.='" + name + "']"));
	}

	public Element sectionLink(String name) {
		return new Element(driver, By.xpath(
				"//a[contains(@class, 'topmenu__link') or contains(@class, 'category__link')][.='" + name + "']"));
	}

	public Element subsectionLink(String name) {
		return new Element(driver, By.xpath("//a[contains(@class, 'topmenu__subitem')][.='" + name + "']"));
	}

	public Element menuItemLink(String name) {
		return new Element(driver,
				By.xpath("//li[contains(@class, 'breadcrumbs__item')][contains(., '" + name + "')]"));
	}

	public Element priceFromInput() {
		return new Element(driver, By.id("glpricefrom"));
	}

	public Element priceToInput() {
		return new Element(driver, By.id("glpriceto"));
	}

	public Element filterByManufacturerLink(String name) {
		return new Element(driver, By.xpath("//input[@name='Производитель " + name + "']/following-sibling::div"));
	}

	public Element firstResultLink() {
		return new Element(driver, By.xpath("//div[contains(@class, 'n-snippet-cell2__title')]/a"));
	}

	public Element productTitle() {
		return new Element(driver,
				By.xpath("//div[contains(@class, 'n-product-title')]//h1[contains(@class, 'title')]"));
	}

	public void waitLoadResults() {
		Element loader = new Element(driver, By.className("preloadable__preloader"));
		loader.waitForPresent();
		loader.waitForNotPresent();
	}
}
