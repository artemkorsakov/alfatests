package ru.alfabank.job.pom;

import org.openqa.selenium.WebDriver;

public abstract class MainPage {
	protected WebDriver driver;

	public MainPage(WebDriver driver) {
		this.driver = driver;
	}
}
