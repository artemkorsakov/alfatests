package ru.alfabank.job.entities;

public enum SearchSystem {
	Google, Yandex, Unknown;

	public static SearchSystem toSearchSystem(String searchSystem) {
		if (searchSystem.contains("google")) {
			return SearchSystem.Google;
		}
		if (searchSystem.contains("yandex")) {
			return SearchSystem.Yandex;
		}
		return SearchSystem.Unknown;
	}
}
