package ru.alfabank.job.stepDefinition;

import static org.junit.Assert.assertEquals;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ru.alfabank.job.entities.StepsContainer;
import ru.alfabank.job.pom.YandexPage;

public class YandexPageSteps {
	private StepsContainer container;

	public YandexPageSteps(StepsContainer container) {
		this.container = container;
	}

	@When("^Перейти в Маркет, раздел \"([^\"]*)\", подраздел \"([^\"]*)\"$")
	public void gotoMarket(String section, String subsection) {
		YandexPage page = new YandexPage(container.driver);
		page.homeLink("Маркет").getWebElement().click();
		page.promoLink().clickIfPresent();
		page.cityLink().clickIfPresent();
		page.sectionLink(section).moveToElement();
		page.subsectionLink(subsection).getDisplayedWebElement().click();
	}

	@When("^Отфильтровать по производителю \"([^\"]*)\"$")
	public void filterByManufacturer(String manufacturer) {
		YandexPage page = new YandexPage(container.driver);
		if (page.menuItemLink(manufacturer).isElementPresent()) {
			return;
		}
		page.filterByManufacturerLink(manufacturer).getWebElement().click();
		page.waitLoadResults();
		page.menuItemLink(manufacturer).waitForPresent();
	}

	@When("^Отфильтровать по цене от ([\\d|\\s]*) рублей$")
	public void filterByPrice(String priceFrom) {
		filterByPrice(priceFrom, "");
	}

	@When("^Отфильтровать по цене от ([\\d|\\s]*) рублей до ([\\d|\\s]*) рублей$")
	public void filterByPrice(String priceFrom, String priceTo) {
		YandexPage page = new YandexPage(container.driver);
		page.priceFromInput().getWebElement().sendKeys(priceFrom);
		page.priceToInput().getWebElement().sendKeys(priceTo);
		page.waitLoadResults();
	}

	@When("^Запомнить наименование первого результата$")
	public void rememberFirstResult() {
		YandexPage page = new YandexPage(container.driver);
		container.firstResult = page.firstResultLink().getWebElement().getText();
	}

	@When("^Перейти по ссылке первого результата$")
	public void clickFirstResult() {
		YandexPage page = new YandexPage(container.driver);
		page.firstResultLink().getWebElement().click();
		page.productTitle().waitForPresent();
	}

	@Then("^Проверить, что отображаемое имя в описании совпадает с тем, которое запомнили$")
	public void assertFirstResult() {
		YandexPage page = new YandexPage(container.driver);
		String actualTitle = page.productTitle().getWebElement().getText();
		assertEquals(container.firstResult, actualTitle);
	}
}
