package ru.alfabank.job.stepDefinition;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import ru.alfabank.job.driverHelper.DriverHelper;
import ru.alfabank.job.entities.SearchSystem;
import ru.alfabank.job.entities.StepsContainer;

public class MainPageSteps {
	private StepsContainer container;
	private Scenario scenario;

	public MainPageSteps(StepsContainer container) {
		this.container = container;
	}

	@Before
	public void before(Scenario scenario) {
		this.scenario = scenario;
	}

	@Given("^Открыть страницу \"([^\"]*)\" в браузере \"([^\"]*)\"$")
	public void openPage(String url, String browser) throws Exception {
		container.driver = DriverHelper.getDriver(browser);
		container.driver.get(url);
		container.searchSystem = SearchSystem.toSearchSystem(url);
	}

	@Given("^Проверить, что открывается сайт \"([^\\\"]*)\"$")
	public void assertUrl(String url) throws Exception {
		String actualUrl = container.driver.getCurrentUrl();
		assertTrue(actualUrl.contains(url));
	}

	@After
	public void afterScenario() {
		if (container.driver == null) {
			return;
		}

		if (!scenario.getStatus().toString().equals("PASSED")) {
			TakesScreenshot scrShot = ((TakesScreenshot) container.driver);
			byte[] data = scrShot.getScreenshotAs(OutputType.BYTES);
			scenario.embed(data, "image/png");
		}

		container.driver.quit();
	}
}
