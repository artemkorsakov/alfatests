package ru.alfabank.job.stepDefinition;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.remote.RemoteWebDriver;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import ru.alfabank.job.entities.SearchSystem;
import ru.alfabank.job.entities.StepsContainer;
import ru.alfabank.job.pom.AlfaPage;

public class AlfaPageSteps {
	private StepsContainer container;

	public AlfaPageSteps(StepsContainer container) {
		this.container = container;
	}

	@When("^Перейти в раздел \"Вакансии\"$")
	public void gotoJobs() {
		AlfaPage page = new AlfaPage(container.driver);
		page.jobsLink().getWebElement().click();
	}

	@When("^На сайте job.alfabank.ru перейди в раздел «О работе в банке»$")
	public void gotoAboutWork() {
		AlfaPage page = new AlfaPage(container.driver);
		page.aboutWorkInBankLink().getWebElement().click();
		page.aboutWorkInBankTitle().waitForPresent();
	}

	@Then("^Сохранить описание работы в банке в файл$")
	public void saveDescription() {
		AlfaPage page = new AlfaPage(container.driver);
		String message = page.message().getWebElement().getText();
		String text = page.info().getWebElement().getText();
		String browser = ((RemoteWebDriver) container.driver).getCapabilities().getBrowserName();
		DateFormat dateFormat = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		Date date = new Date();
		String currentDate = dateFormat.format(date);
		SearchSystem searchSystem = container.searchSystem;

		System.out.println(message);
		System.out.println(text);
		System.out.println(browser);
		System.out.println(searchSystem);
		System.out.println(currentDate);

		StringBuilder filename = new StringBuilder();
		filename.append("./TestFiles/");
		filename.append(currentDate);
		filename.append("_");
		filename.append(browser);
		filename.append("_");
		filename.append(searchSystem);
		filename.append(".txt");

		try (BufferedWriter writer = new BufferedWriter(
				new OutputStreamWriter(new FileOutputStream(filename.toString()), "UTF8"))) {
			writer.write(message + System.lineSeparator() + text);
			writer.flush();
		} catch (IOException ex) {
			System.out.println(ex.getMessage());
		}
	}
}
