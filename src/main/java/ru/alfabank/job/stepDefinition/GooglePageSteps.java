package ru.alfabank.job.stepDefinition;

import cucumber.api.java.en.When;
import ru.alfabank.job.entities.StepsContainer;
import ru.alfabank.job.pom.GooglePage;

public class GooglePageSteps {
	private StepsContainer container;

	public GooglePageSteps(StepsContainer container) {
		this.container = container;
	}

	@When("^Ввести в поиске \"([^\"]*)\" и нажать найти$")
	public void search(String text) {
		GooglePage page = new GooglePage(container.driver);
		page.searchInput().getWebElement().sendKeys(text);
		page.clickFirstResult().getWebElement().click();
	}

	@When("^Перейти на сайт Альфа-Банка$")
	public void gotoAlfa() {
		GooglePage page = new GooglePage(container.driver);
		page.alfaLink().getWebElement().click();
	}
}
