package ru.alfabank.job.driverHelper;

public final class BrowserTypeExtensions {
	public static BrowserType ToBrowserType(String browser) throws Exception {
		switch (browser) {
		case "Firefox":
			return BrowserType.FIREFOX;
		case "Chrome":
			return BrowserType.CHROME;
		case "Opera":
			return BrowserType.OPERA;
		case "IE":
			return BrowserType.IE;
		case "Edge":
			return BrowserType.EDGE;
		default:
			throw new Exception("Unknown browser" + browser);
		}
	}
}
