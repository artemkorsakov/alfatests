package ru.alfabank.job.driverHelper;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;

public final class DriverHelper {
	public static WebDriver getDriver(String browser) throws Exception {
		BrowserType type = BrowserTypeExtensions.ToBrowserType(browser);
		return DriverHelper.getDriver(type);
	}

	public static WebDriver getDriver(BrowserType type) {
		WebDriver driver = createDriver(type);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		return driver;
	}

	private static WebDriver createDriver(BrowserType type) {
		switch (type) {
		case FIREFOX:
			return new FirefoxDriver();
		case CHROME:
			return new ChromeDriver();
		case OPERA:
			OperaOptions options = new OperaOptions();
			options.setBinary("C:\\Program Files\\Opera\\53.0.2907.110\\opera.exe");
			return new OperaDriver(options);
		case IE:
			return new InternetExplorerDriver();
		case EDGE:
			return new EdgeDriver();
		default:
			return null;
		}
	}
}
