package ru.alfabank.job.driverHelper;

public enum BrowserType {
	FIREFOX, CHROME, OPERA, IE, EDGE
}
