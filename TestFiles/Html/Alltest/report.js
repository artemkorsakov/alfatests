$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("Feature/Task2.feature");
formatter.feature({
  "name": "Задание 2",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Кейс 1. Проверка смартфонов",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Открыть страницу \"https://yandex.ru/\" в браузере \"Chrome\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MainPageSteps.openPage(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти в Маркет, раздел \"Электроника\", подраздел \"Мобильные телефоны\"",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.gotoMarket(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Отфильтровать по производителю \"Samsung\"",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.filterByManufacturer(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Отфильтровать по цене от 40 000 рублей",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.filterByPrice(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Запомнить наименование первого результата",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.rememberFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти по ссылке первого результата",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.clickFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Проверить, что отображаемое имя в описании совпадает с тем, которое запомнили",
  "keyword": "Then "
});
formatter.match({
  "location": "YandexPageSteps.assertFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "Кейс 2. Проверка наушников",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Открыть страницу \"https://yandex.ru/\" в браузере \"Chrome\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MainPageSteps.openPage(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти в Маркет, раздел \"Электроника\", подраздел \"Наушники\"",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.gotoMarket(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Отфильтровать по производителю \"Beats\"",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.filterByManufacturer(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Отфильтровать по цене от 17 000 рублей до 25 000 рублей",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.filterByPrice(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Запомнить наименование первого результата",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.rememberFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти по ссылке первого результата",
  "keyword": "When "
});
formatter.match({
  "location": "YandexPageSteps.clickFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Проверить, что отображаемое имя в описании совпадает с тем, которое запомнили",
  "keyword": "Then "
});
formatter.match({
  "location": "YandexPageSteps.assertFirstResult()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("Feature/Task3.feature");
formatter.feature({
  "name": "Задание 2",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "Кейс 1. Проверка смартфонов",
  "description": "",
  "keyword": "Scenario"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "Открыть страницу \"https://www.google.com\" в браузере \"Chrome\"",
  "keyword": "Given "
});
formatter.match({
  "location": "MainPageSteps.openPage(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Ввести в поиске \"официальный сайт альфа банка\" и нажать найти",
  "keyword": "When "
});
formatter.match({
  "location": "GooglePageSteps.search(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти на сайт Альфа-Банка",
  "keyword": "When "
});
formatter.match({
  "location": "GooglePageSteps.gotoAlfa()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Перейти в раздел \"Вакансии\"",
  "keyword": "When "
});
formatter.match({
  "location": "AlfaPageSteps.gotoJobs()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Проверить, что открывается сайт \"job.alfabank.ru\"",
  "keyword": "Then "
});
formatter.match({
  "location": "MainPageSteps.assertUrl(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "На сайте job.alfabank.ru перейди в раздел «О работе в банке»",
  "keyword": "When "
});
formatter.match({
  "location": "AlfaPageSteps.gotoAboutWork()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Сохранить описание работы в банке в файл",
  "keyword": "Then "
});
formatter.match({
  "location": "AlfaPageSteps.saveDescription()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});